/**
 * Класс шаблонизатора
 */
function Templater(template)
{
    var thisObj = this;

    /**
     * Путь файла шаблона
     * @var string
     */
    var _template = null;

    /**
     * Конструктор класса
     * @param string msg сообщение об ошибке
     */
    {
        _template = template;
    }


    /**
     * Наполняет шаблон реальными данными
     * @param JSON renderingData объект вида ключ-текст для наполнения шаблона
     * @return Blob объект ZIP-файла
     */
    this.render = function(renderingData)
    {
        var dO = $.Deferred();
        JSZipUtils.getBinaryContent(_template, function(err, data) {
            if (err) {
                throw err; // or handle err
            }
            var zip = new JSZip(data);
            var template = zip.file('word/document.xml').asText();
            var rendered = Mustache.render(template, renderingData);
            zip.file('word/document.xml', rendered);
            var content = zip.generate({ type: 'blob', compression: 'DEFLATE' });
            dO.resolve(content);
        });
        return dO.promise();
    }
}