jQuery(document).ready(function($) {
    $('body').append(
        '<div style="position: absolute; left: 50%; width: 200px; text-align: center; margin-left: -100px; top: 0; background: white; padding: 10px; ">' +
        '  <a href="#" data-role="ksi-get-act" style="display: inline-block; margin-right: 10px">Акт</a> ' +
        '  <a href="#" data-role="ksi-get-accept" style="display: inline-block; margin-right: 10px">Предоставление</a> ' +
        '  <a href="#" data-role="ksi-get-reject" style="display: inline-block;">Отказ</a> ' +
        '</div>'
    );

    var getMainData = function()
    {
        var data = {};
        data.FULL_NAME = $('.x15.x4z:contains("Наименование")').next().text();
        data.SHORT_NAME = $('.x15.x4z:contains("Сокращенное наименование")').next().text();
        data.BRAND_NAME = $('.x15.x4z:contains("Фирменное наименование")').next().text();
        data.JURIDICAL_ADDRESS = $('.x15.x4z:contains("Адрес")').next().text();
        data.INN = $('.x15.x4z:contains("ИНН")').next().text();
        data.OGRN = $('.x15.x4z:contains("ОГРН(ИП)")').next().text();
        data.NUM = $('.x15.x4z:contains("Номер дела заявителя")').next().text();
        return data;
    }


    $('[data-role="ksi-get-act"]').on('click', function() {
        var data = getMainData();
        var dt = new Date();
        data.DATETIME = date('d.m.Y H ч. i мин.')
        var filename = data.SHORT_NAME.replace(/[^\w А-Яа-я\-\.\,]+/gi, '') + '.docx';
        var t = new Templater(chrome.extension.getURL('act.docx'));
        var dO = t.render(data);
        dO = dO.then(function(result) { saveAs(result, filename); });
    })
})