function ErrorViewTest()
{
    this.testShow = function()
    {
        var msg = 'Test Alert';
        var e = new ErrorView(msg);
        e.show();
        console.log('ErrorView.show OK');
    };
}


function TemplaterTest()
{
    this.testRender = function()
    {
        var t = new Templater('template.docx');
        var dO = t.render({ VARIABLE1: 'aaa', VARIABLE2: 'bbb', VARIABLE3: 'ccc' });
        dO = dO.then(function(result) {
            var dO = new $.Deferred();
            var reader = new FileReader();
            reader.onload = function() {
                dO.resolve(reader.result);
            };
            reader.readAsArrayBuffer(result);
            return dO;
        });
        dO = dO.then(function(result) {
            var dO = new $.Deferred();
            var zip = new JSZip(result);
            var rendered = zip.file('word/document.xml').asText();
            if (/aaa/.test(rendered) && /bbb/.test(rendered) && /ccc/.test(rendered)) {
                console.log('Templater.render OK');
            } else {
                throw new Error('Invalid rendering of template');
            }
            zip.file('word/document.xml', rendered);
            var content = zip.generate({ type: 'blob' });
            dO.resolve(content);
        });
    };
}



jQuery(document).ready(function($) {
    $('[data-role="ErrorView.show"]').on('click', function() {
        var ev = new ErrorViewTest();
        ev.testShow();
    });


    $('[data-role="Templater.render"]').on('click', function() {
        var t = new TemplaterTest();
        t.testRender();
    });


    $('[data-role="saveAs"]').on('click', function() {
        var t = new Templater('template.docx');
        var dO = t.render({ VARIABLE1: 'aaa', VARIABLE2: 'bbb', VARIABLE3: 'ccc' });
        dO = dO.then(function(result) {
            saveAs(result, 'rendered.docx');
        });
    });
});

